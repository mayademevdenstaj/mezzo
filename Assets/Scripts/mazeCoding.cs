﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class mazeCoding : MonoBehaviour {

	public GameObject guardGameObject;
	//public bool debugMode = false;
	public bool createMazeBool = false;
	//public bool saveMazeBool = false;
	public bool destroyMazeBool = false;
	public bool nextLevel;
	public int size;
	public int levelNum;
	public string[] de;
	TextAsset levelText;

	void Awake(){
		// eğer debug mode değil ise level num'ı player prefs den alıp maze'i spawn eder
		//if (!debugMode) {
			levelNum = PlayerPrefs.GetInt ("playedLevel");
			createMaze ();
		//}

		if (createMazeBool)
			createMaze ();
		createMazeBool = false;
	}

	void Update(){
		if (nextLevel) {
			destroyMazeBool = true;

			levelNum++;
			createMazeBool = true;
		}
		if (destroyMazeBool)
			destroyMaze ();
		if (createMazeBool) {
			createMaze ();
		}
		/*if (saveMazeBool) {
			saveMaze ();
		}*/


		nextLevel = false;
		createMazeBool = false;
		//saveMazeBool = false;
		destroyMazeBool = false;
	}

	/*void saveMaze (){
		levelText = Resources.Load ("levelData") as TextAsset;
		string data = levelText.text;

		GameObject[] guards = GameObject.FindGameObjectsWithTag ("Guard");
		foreach (GameObject g in guards) {
			data += Mathf.Round(g.transform.position.x).ToString () + "," +  g.GetComponent<guardian> ().row.ToString () + "," + Mathf.Round( g.transform.position.z).ToString() + "G";
		}
		data += "L";
		File.WriteAllText("Assets/Resources/levelData.txt",data);
	}*/

	void createMaze(){
		levelText = Resources.Load ("levelData") as TextAsset;
		string data = levelText.text;

		GetComponent<MazeSpawner> ().candyCount = candys ();
        GetComponent<MazeSpawner>().itemCount = (levelNum/10)+1;
		GetComponent<MazeSpawner> ().Rows = size;
		GetComponent<MazeSpawner> ().Columns = size;
		GetComponent<MazeSpawner> ().RandomSeed = levelNum + 12345678;
		GetComponent<MazeSpawner> ().genMaze ();

		string[] levs = data.Split ('L');
		if (levs [levelNum].Contains ("G")) {
			string[] guards = levs [levelNum].Split ('G');

			foreach (string g in guards) {
				if (g != "") {
					GameObject grd = Instantiate (guardGameObject, new Vector3 (float.Parse (g.Split (',') [0]), 1f, float.Parse (g.Split (',') [2])), Quaternion.Euler (90f, 0f, 0f)) as GameObject;
					grd.transform.parent = transform;
					grd.GetComponent<guardian> ().row = float.Parse (g.Split (',') [1]);
				}
			}
		}
	}

	int candys (){
		if (levelNum / 10f <= 1f)
			size = 12;
		else if (levelNum / 10f <= 2f)
			size = 15;
		else if (levelNum / 10f <= 3f)
			size = 18;
		else if (levelNum / 10f <= 4f)
			size = 21;
		else if (levelNum / 10f <= 5f)
			size = 24;
		else if (levelNum / 10f <= 6f)
			size = 27;
		return (size * size) / 10;
	}

	public void destroyMaze(){
		for (int i = transform.childCount - 1; i >= 0; i--) {
			Destroy(transform.GetChild (i).gameObject);
		}
	}
}