﻿using UnityEngine;
using System.Collections;

public class guardian : MonoBehaviour {

	public float row; //0=row, 1=column
    public Vector2 finalSwipe;
    Ray upRay;
    Ray downRay;
    Ray rightRay;
    Ray leftRay;
	float crashedSec;
    RaycastHit hit;
	public Animator anim;
	int whichWay; // 0=up, 1=down, 2=right, 3=left

	public bool move = true;

	void Start () {
		crashedSec = -1;
		if (move) {
			if (row == 0) {
				goRight ();
			} 
			else {
				goUp ();
			}
		}
	}
	
	void FixedUpdate () {
		if (move && Time.timeSinceLevelLoad - crashedSec > 0.5f) {
			if (row == 0) {
				if (whichWay == 2) {
					if(Time.timeSinceLevelLoad - crashedSec > 1f)
					rightRay = new Ray (transform.position, Vector3.right);

					leftRay = new Ray (transform.position, Vector3.left);
				}
				else //if (whichWay == 3)
				{
					if(Time.timeSinceLevelLoad - crashedSec > 1f)
					leftRay = new Ray (transform.position, Vector3.left);
					rightRay = new Ray (transform.position, Vector3.right);
					
				}

				if (Physics.Raycast (rightRay, out hit, 2) == true) {
					goLeft ();
					crashedSec = Time.timeSinceLevelLoad;
				}
				else if (Physics.Raycast (leftRay, out hit, 2) == true)
				{
					goRight ();
					crashedSec = Time.timeSinceLevelLoad;
				}
				anim.SetFloat ("motionY", finalSwipe.y);
				anim.SetFloat ("motionX", finalSwipe.x);
				transform.Translate (new Vector3 (finalSwipe.x, 0, finalSwipe.y));
			} 
			else {
				//upRay = new Ray (transform.position, Vector3.forward);
				//downRay = new Ray (transform.position, Vector3.back);

				if (whichWay == 0) {
					if(Time.timeSinceLevelLoad - crashedSec > 1f)
					upRay = new Ray (transform.position, Vector3.forward);
					downRay = new Ray (transform.position, Vector3.back);
				}
				else //if (whichWay == 1)
				{
					upRay = new Ray (transform.position, Vector3.forward);
					if(Time.timeSinceLevelLoad - crashedSec > 1f)
					downRay = new Ray (transform.position, Vector3.back);
				}

				if (Physics.Raycast (upRay, out hit, 2) == true) {
					goDown ();
					crashedSec = Time.timeSinceLevelLoad;
				} 
				else if (Physics.Raycast (downRay, out hit, 2) == true) {
					goUp ();
					crashedSec = Time.timeSinceLevelLoad;
				}
				anim.SetFloat ("motionY", finalSwipe.y);
				anim.SetFloat ("motionX", finalSwipe.x);
				transform.Translate (new Vector3 (finalSwipe.x, finalSwipe.y, 0));
			}
		}
	}

    void goRight()
    {
        finalSwipe = new Vector2(0.1f, 0);
		whichWay = 2;
    }
    void goLeft()
    {
        finalSwipe = new Vector2(-0.1f, 0);
		whichWay = 3;
    }
    void goUp()
    {
        finalSwipe = new Vector2(0, 0.1f);
		whichWay = 0;
    }
    void goDown()
    {
        finalSwipe = new Vector2(0, -0.1f);
		whichWay = 1;
    }
}
