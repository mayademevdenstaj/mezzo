﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class playerController : MonoBehaviour {

	public List<GameObject> pathDebug;
    public static List<GameObject> path;
	List<GameObject> memPath;
    public pathFinderCube pathfinder;
    public bool moveStart = false;
	public static int currentPath = 0;
	public float speed = 0.4f;
	public float crashSpeed = 0.1f;
	public bool gameOver = false;
	public bool crashed = false;
	public Texture floorTexture;
	Vector3 vel;
	//public Animator anim;
	public GameObject test;
	public bool levelEnd = false;
	public GameObject medalWarning;
	AudioSource aud;
	public GameObject firstPath;
	public Texture mazeTexture;
    public bool tutorial;
	public GameManager gameMan;
    public Vector3[] startCoordinates;
    public int tutCount;
    public GameObject slime;

    public List<List<Sprite>> allChars;
    public List<Sprite> char1Sptrites;
    public List<Sprite> char2Sptrites;
    public List<Sprite> char3Sptrites;
    public List<Sprite> char4Sptrites;
    List<Sprite> moveSprites;
    int showSprite;
	float totalAnimTime;
	float lastAnimTime;

	void Start(){
		aud = GetComponent<AudioSource> ();
		path = new List<GameObject>();
		memPath = new List<GameObject>();
        allChars = new List<List<Sprite>>();
        allChars.Add(char1Sptrites);
        allChars.Add(char2Sptrites);
        allChars.Add(char3Sptrites);
        allChars.Add(char4Sptrites);
        showSprite = 0;
        currentPath = 0;
		path.Add (firstPath);
        moveStart = false;
		Time.timeScale = 1f;
        //animSprite();
        if (PlayerPrefs.GetInt("tutorial") == 1)
        {
            tutCount = 0;
            transform.position = startCoordinates[tutCount];
            PlayerPrefs.SetInt("controls", 1);
        }
        
	}

	void FixedUpdate () 
	{
			if (moveStart && !GameManager.win) {
				if (crashed)
					transform.position = new Vector3 (Mathf.Lerp (transform.position.x, path [currentPath].transform.position.x, crashSpeed), 0, Mathf.Lerp (transform.position.z, path [currentPath].transform.position.z, crashSpeed));
				else if (!levelEnd)
					transform.position = Vector3.MoveTowards (transform.position, path [currentPath].transform.position, speed);
			}
			if (currentPath < path.Count - 1 && Vector3.Distance (path [currentPath].transform.position, transform.position) < 1f && !levelEnd) {
				test = path [currentPath + 1];
			}

		if (currentPath < path.Count - 1 && moveStart && !gameOver && Vector3.Distance (path [currentPath].transform.position, transform.position) < 0.3f && Time.timeSinceLevelLoad > 0.1f) {
			currentPath++;
		}
        else if (currentPath == path.Count - 1 && pathfinder.waitForPlayersMove)
        {
			pathfinder.waitForPlayersMove = false;
			pathfinder.transform.position = new Vector3 (path[currentPath].transform.position.x,2f,path[currentPath].transform.position.z);
		}
			// Ses
			if (currentPath == path.Count - 1)
				aud.volume = 0.1f;
			else if (moveStart)
				aud.volume = 0.8f;
		pathDebug = path;

		totalAnimTime += Time.deltaTime;
		if (totalAnimTime - lastAnimTime >= 0.1f) {
			lastAnimTime = totalAnimTime;
			animSprite ();
		}

	}

    public void animSprite()
    {
        if (transform.position.z - path[currentPath].transform.position.z < 0)
            moveSprites = allChars[PlayerPrefs.GetInt("currentChar") - 1].GetRange(3, 3);
        else if (transform.position.x - path[currentPath].transform.position.x < 0)
            moveSprites = allChars[PlayerPrefs.GetInt("currentChar") - 1].GetRange(9, 3);
        else if (transform.position.x - path[currentPath].transform.position.x > 0)
            moveSprites = allChars[PlayerPrefs.GetInt("currentChar") - 1].GetRange(6, 3);
        else
            moveSprites = allChars[PlayerPrefs.GetInt("currentChar") - 1].GetRange(0, 3);

        slime.GetComponent<SpriteRenderer>().sprite = moveSprites[showSprite];
        showSprite++;
        if (showSprite == 3)
            showSprite = 0;
    }

    void finishAnim()
    {
        slime.transform.localScale = new Vector3(slime.transform.localScale.x - 0.1f, slime.transform.localScale.y - 0.1f, slime.transform.localScale.z - 0.1f);
        if (slime.transform.localScale.x < 0.1)
        {
            Time.timeScale = 0f;
            GameManager.win = true;
            GameManager.starIncrease = true;
        }
        else
        {
            Invoke("finishAnim", 0.1f);
        }
    }

    void OnTriggerEnter(Collider col) 
    {
		if (col.gameObject.name == "King(Clone)") {
			if (PlayerPrefs.GetInt ("tutorial") == 0 || tutCount == 3) {
				currentPath--;

				memPath = path.GetRange (0, currentPath + 1);

				int count = 0;
				foreach (GameObject i in path) {
					if (i == path [currentPath + 1])
						count++;
					if (count == 2)
						break;
				}
				moveStart = false;
				path = memPath;
				crashed = true;
				gameOver = true;
				levelEnd = true;
				finishAnim ();
			} 
			else { //tutorial
				if (tutCount < 3) {

					Invoke ("clearTrail", 0.1f);

					tutCount++;
					if (tutCount == 1)
						PlayerPrefs.SetInt ("controls", 0);
					if (tutCount == 3)
						PlayerPrefs.SetInt ("tutorial", 0);

					gameMan.GetComponent<UIelements> ().nextTutorial ();
                    
					pathfinder.transform.position = new Vector3 (startCoordinates [tutCount].x, 2, startCoordinates [tutCount].z);
					pathfinder.l = true;
					pathfinder.r = false;
					pathfinder.f = true;
					pathfinder.b = true;
					pathfinder.setProb ();
					transform.position = startCoordinates [tutCount];
					currentPath = path.Count;
				} else
					GameManager.win = true;
			}
		} 
		else if (col.gameObject.name == "Guard(Clone)") {
			if (!tutorial)
				gameMan.loseGame ();
			else {
				pathfinder.gameObject.transform.position = new Vector3 (startCoordinates [tutCount].x, 2f, startCoordinates [tutCount].z);
				pathfinder.l = true;
				pathfinder.r = false;
				pathfinder.f = true;
				pathfinder.b = true;
				pathfinder.setProb ();
				transform.position = startCoordinates [tutCount];
				currentPath = path.Count;
			}
		}
		else if (col.gameObject.name == "Wall(Clone)" && !crashed) {
			
			currentPath--;
			gameOver = true;
			memPath = path.GetRange (0, currentPath + 1);
			pathfinder.trailRenderer.Clear ();
			path = memPath;
			crashed = true;
		} 
		else if (col.gameObject.name == "medal(Clone)") {
			gameMan.peekCount++;
			gameMan.peekText.text = String.Format ("Peek ({0})", gameMan.peekCount);
			gameMan.medalSoundPlay ();
		} 
		else if (col.gameObject.name == "coin(Clone)") {
			gameMan.coinSoundPlay ();
		}
    }

	public void die(){
		transform.localScale = new Vector3(1f,1f,0.4f);
		//Destroy (anim);
	}

	public void clearTrail(){
		pathfinder.transform.GetChild (0).GetComponent<TrailRenderer> ().Clear ();
    }

}
