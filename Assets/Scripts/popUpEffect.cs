﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class popUpEffect : MonoBehaviour {

	public Vector3 targetPos;
	public bool startGo = false;
	public float smooth = 0.1f;
	public bool goTransparent = false;
	public float transparentChannel;
	public popUpEffect motionParent;

	public void popUp () {
		startGo = true;
	}
	public void Update(){
		if (startGo) {
			if (goTransparent) {
				GetComponent<Image>().color = new Color (0, 0, 0, Mathf.Lerp (GetComponent<Image>().color.a, transparentChannel, 0.04f));
				if (GetComponent<Image> ().color.a > transparentChannel-0.3f && motionParent)
					motionParent.popUp ();
			} 
			else
				GetComponent<RectTransform> ().position = new Vector3 (Mathf.Lerp (GetComponent<RectTransform> ().position.x, targetPos.x + Screen.width / 2, smooth), Mathf.Lerp (GetComponent<RectTransform> ().position.y, targetPos.y + Screen.height / 2, smooth), 0);
		}
	}
}
