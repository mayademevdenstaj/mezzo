﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class UIelements : MonoBehaviour {

	public AudioMixerGroup sound;
	public AudioMixerGroup music;
	public GameObject SFXbutt;
	public GameObject musicButt;
	public pathFinderCube pathfinder;
	public GameObject drawOk;
	public GameObject swipeOk;
    public GameObject tutorialWindow1;
    public GameObject tutorialWindow2;
    public GameObject tutorialWindow3;
	public GameObject levelSelect;
	public GameManager gameMan;
    int tutCount = 0;
    public bool mainMen = false;

	void Start(){
		updateSettings ();
	}

	public void soundOn()
	{ 
		PlayerPrefs.SetFloat("SFX", 0);
		sound.audioMixer.SetFloat ("SFX", 0f);
	}
	public void soundOff()
	{ 
		PlayerPrefs.SetFloat("SFX", -80f);
		sound.audioMixer.SetFloat ("SFX", -80f);
	}

	public void musicOn()
	{ 
		PlayerPrefs.SetFloat("music", 0);
		music.audioMixer.SetFloat ("music", 0f);
	}
	public void musicOff()
	{ 
		PlayerPrefs.SetFloat("music", -80f);
		music.audioMixer.SetFloat ("music", -80f);
	}
	public void updateSettings(){
		if (mainMen || (GetComponent<GameManager>() && !GetComponent<GameManager>().tutorialLevel)){
            if (PlayerPrefs.GetInt("controls") == 1)
            {
                swipeOk.SetActive(true);
                drawOk.SetActive(false);
            }
            else
            {
                swipeOk.SetActive(false);
                drawOk.SetActive(true);
            }

            music.audioMixer.SetFloat("music", PlayerPrefs.GetFloat("music"));
            sound.audioMixer.SetFloat("SFX", PlayerPrefs.GetFloat("SFX"));
            if (PlayerPrefs.GetFloat("music") == 0f)
                musicButt.SetActive(false);
            else
                musicButt.SetActive(true);

            if (PlayerPrefs.GetFloat("SFX") == 0f)
                SFXbutt.SetActive(false);
            else
                SFXbutt.SetActive(true);
        }
	}

	public void swipeControl(){
		PlayerPrefs.SetInt ("controls", 1);
	}
	public void drawControl(){
		PlayerPrefs.SetInt ("controls",0);
	}

    public void nextTutorial() {

		gameMan.openPopup ();

        tutCount++;
        if (tutCount == 1)
            tutorialWindow1.SetActive(true);
        if (tutCount == 2)
            tutorialWindow2.SetActive(true);
        if (tutCount == 3)
            tutorialWindow3.SetActive(true);
    }
    public void goTutLevel() {
        SceneManager.LoadScene(2);
    }

	public void startButton()
	{
		if (PlayerPrefs.GetInt ("tutorial") == 1)
			goTutLevel ();
		else
			levelSelect.SetActive (true);
	}
}
