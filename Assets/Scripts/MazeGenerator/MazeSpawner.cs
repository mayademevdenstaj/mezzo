﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Game object, that creates maze and instantiates it in scene
public class MazeSpawner : MonoBehaviour {
	public enum MazeGenerationAlgorithm{
		RecursiveTree,
	}
	public bool FullRandom = false;
	public int RandomSeed = 12345;
	public GameObject Floor = null;
	public GameObject Wall;
	public GameObject King;
	public int Rows = 5;
	public int Columns = 5;
	public float CellWidth = 5;
	public float CellHeight = 5;
	public bool AddGaps = true;
	public GameObject GoalPrefab = null;
    public GameObject peekPrefab = null;
	public int spawn_row, spawn_col;
	public static List<string> addedObjects; //rowcolumn
	public int candyCount = 9;
    public int itemCount;
	public bool spawnMaze;

	GameManager gmn = new GameManager ();
	public Sprite s1Wall;
	public Sprite s3Wall;
	public Sprite s5Wall;
	public Sprite s1Item;
	public Sprite s3Item;
	public Sprite s5Item;

	private BasicMazeGenerator mMazeGenerator = null;

	void wallSpriteFind ()
	{
		int playedLevelInt = PlayerPrefs.GetInt ("playedLevel"); 
		if (playedLevelInt < 11) {
			Wall.GetComponent<SpriteRenderer>().sprite = s1Wall;
			GoalPrefab.GetComponent<SpriteRenderer> ().sprite = s1Item;

		}
		else if (playedLevelInt < 20) {
			Wall.GetComponent<SpriteRenderer>().sprite = s1Wall;
			GoalPrefab.GetComponent<SpriteRenderer> ().sprite = s1Item;
		}
		else if (playedLevelInt < 30) {
			Wall.GetComponent<SpriteRenderer>().sprite = s3Wall;
			GoalPrefab.GetComponent<SpriteRenderer> ().sprite = s3Item;
		}
		else if (playedLevelInt < 40) {
			Wall.GetComponent<SpriteRenderer>().sprite = s3Wall;
			GoalPrefab.GetComponent<SpriteRenderer> ().sprite = s3Item;
		}
		else if (playedLevelInt < 50) {
			Wall.GetComponent<SpriteRenderer>().sprite = s5Wall;
			GoalPrefab.GetComponent<SpriteRenderer> ().sprite = s5Item;
		}
		else if (playedLevelInt < 60) {
			Wall.GetComponent<SpriteRenderer>().sprite = s5Wall;
			GoalPrefab.GetComponent<SpriteRenderer> ().sprite = s5Item;
		}
	}

	void Update(){
		if (spawnMaze)
			
			genMaze ();
		spawnMaze = false;
	}

	public void genMaze () {
		addedObjects = new List<string>();

		if (!FullRandom) {
			Random.InitState (RandomSeed);
			//Random.seed = RandomSeed;
		}
			mMazeGenerator = new RecursiveTreeMazeGenerator (Rows, Columns);

		mMazeGenerator.GenerateMaze ();

		int col_or_row = Random.Range (0, 2); //1 means last column, 0 means first row

		if (col_or_row == 0) {
			spawn_row = Rows - 1;
			spawn_col = Random.Range (0, Columns);
		} 
		else if (col_or_row == 1) {
			spawn_row = Random.Range (0, Rows);
			spawn_col = Columns - 1;
		
		}
			
		GameObject fmps;
		wallSpriteFind ();
		for(int i = 0; i < Columns; i++){
			fmps = Instantiate(Wall,new Vector3(-2,0,i *4)+Wall.transform.position,Quaternion.Euler(90,270,0)) as GameObject;// front
			fmps.transform.parent = transform;
			fmps.GetComponent<SpriteRenderer>().sortingOrder = 2;
		}
		for(int i = 0; i < Rows; i++){
			fmps = Instantiate(Wall,new Vector3(i*4,0,-2)+Wall.transform.position,Quaternion.Euler(90,180,0)) as GameObject;// front
			fmps.transform.parent = transform;
			fmps.GetComponent<SpriteRenderer>().sortingOrder = 2;
		}

		for (int row = 0; row < Rows; row++) {
			for(int column = 0; column < Columns; column++){
				
				float x = column*(CellWidth+(AddGaps?.2f:0));
				float z = row*(CellHeight+(AddGaps?.2f:0));
				MazeCell cell = mMazeGenerator.GetMazeCell(row,column);
				GameObject tmp;


				tmp = Instantiate(Floor,new Vector3(x,0,z), Quaternion.Euler(90,0,0)) as GameObject;
				tmp.transform.parent = transform;

				if (row == spawn_row && column == spawn_col) // respawn a king
				{
					tmp = Instantiate(King, new Vector3(x,1,z), Quaternion.Euler(0,0,0)) as GameObject;
					tmp.transform.parent = transform;
					addedObjects.Add(row.ToString()+ column.ToString());
				}

				if(cell.WallRight){
					tmp = Instantiate(Wall,new Vector3(x+CellWidth/2,0,z)+Wall.transform.position,Quaternion.Euler(90,90,0)) as GameObject;// right
					tmp.transform.parent = transform;
					tmp.GetComponent<SpriteRenderer>().sortingOrder = 1;
				}
				/*
				if(cell.WallLeft){
					tmp = Instantiate(Wall,new Vector3(x-CellWidth/2,0,z)+Wall.transform.position,Quaternion.Euler(90,270,0)) as GameObject;// left
					tmp.transform.parent = transform;
					tmp.GetComponent<SpriteRenderer>().sortingOrder = 4;
				}
				*/
				if(cell.WallFront){
					tmp = Instantiate(Wall,new Vector3(x,0,z+CellHeight/2)+Wall.transform.position,Quaternion.Euler(90,0,0)) as GameObject;// front
					tmp.transform.parent = transform;
					tmp.GetComponent<SpriteRenderer>().sortingOrder = 2;
				}
				/*
				if(cell.WallBack){
					tmp = Instantiate(Wall,new Vector3(x,0,z-CellHeight/2)+Wall.transform.position,Quaternion.Euler(90,180,0)) as GameObject;// back
					tmp.transform.parent = transform;
					tmp.GetComponent<SpriteRenderer>().sortingOrder = 2;
				}
				*/
			}
		}
		//candies
		generator(GoalPrefab, candyCount);
		//medals
		generator(peekPrefab, itemCount);
	}

	void generator(GameObject generate, int genCount)
	{
		int currentRow = 0;
		int currentColumn = 0;
		string currentCell = "";

		for(int i=0;i<genCount;i++)
		{
			while(true)
			{
				currentRow = Random.Range (1, Rows);
				currentColumn = Random.Range (1, Columns);

				currentCell = (currentColumn.ToString () + currentRow.ToString ());

				int count=0;
				foreach (string j in addedObjects)
				{
					if (currentCell == j)
						count++;
				}

				if (count ==0)
					break;
			}

			addedObjects.Add(currentCell);
			addedObjects.Add ((currentColumn + 1).ToString () + currentRow.ToString ()); //sağ
			addedObjects.Add ((currentColumn + 1).ToString () + (currentRow + 1).ToString ()); //sağ üst
			addedObjects.Add (currentColumn.ToString () + (currentRow + 1).ToString ()); //üst

			addedObjects.Add ((currentColumn - 1).ToString () + currentRow.ToString ()); // sol
			addedObjects.Add ((currentColumn - 1).ToString () + (currentRow - 1).ToString ()); // sol alt
			addedObjects.Add (currentColumn.ToString () + (currentRow - 1).ToString ()); // alt

			addedObjects.Add ((currentColumn + 1).ToString () + (currentRow - 1).ToString ()); //sağ alt
			addedObjects.Add ((currentColumn - 1).ToString () + (currentRow + 1).ToString ()); //sol üst

			GameObject tmp = Instantiate(generate,new Vector3(currentRow*(CellHeight+(AddGaps?.2f:0)),1, currentColumn*(CellWidth+(AddGaps?.2f:0))), Quaternion.Euler(90,0,0)) as GameObject;
			tmp.transform.parent = transform;
		}
	}
}
