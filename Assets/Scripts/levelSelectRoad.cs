﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class levelSelectRoad : MonoBehaviour {
	int i;
	public GameObject[] levelButtons;
	public int plus;
	public int low;

	void Start () {
		foreach (GameObject g in levelButtons) {
			i++;
			g.transform.GetChild (0).GetComponent<Text> ().text = ((low - i) + plus).ToString ();
		}
	}
}
