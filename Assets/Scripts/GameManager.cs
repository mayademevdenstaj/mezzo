﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class GameManager : MonoBehaviour {

	public static bool popUpOn = false;

	public AudioSource loseSound;
	public AudioSource slimeDieSound;
	public AudioSource winSound;
	public AudioSource coinSound;
	public AudioSource medalSound;

	public static bool win = false;
    public bool tutorialLevel;
    public static int collected = 0; //collected candy count
	public float time;
	public bool started = false;
	public static int turnNo = 0;
	public static bool starIncrease = false;
	bool canStart = false;
	bool canRestart = false;
	public int peekCount = 3;
	public static bool canPeek = true;
	public float timeDeb;
	public float plusTime;
	public GameObject peekTutorial;
	bool  gameLost = false;

	public GameObject loseScreen;
	public GameObject youWinScreen;
	public GameObject pauseScreen;
	public GameObject nextButton;
	public sceneTrans fader;
	public GameObject trailRenderer;

	[System.Serializable]
	public class starClass
	{
		public Text starText;
		public GameObject star1;
		public GameObject star2;
		public GameObject star3;
		public Image starTimestar1;
		public Image starTimestar2;
		public Image starTimestar3;
	}

	[System.Serializable]
	public class levelGraphics
	{
		public GameObject s1Objects;
		public Material s1trailMaterial;

		public GameObject s2Objects;

		public GameObject s3Objects;
		public Material s3trailMaterial;

		public GameObject s4Objects;

		public GameObject s5Objects;
		public Material s5trailMaterial;

		public GameObject s6Objects;
	}

	[System.Serializable]
	public class starTimesClass
	{
		public float star3time;
		public float star2time;
		public float star1time;
	}

	public int[] peekCounts;
	public starTimesClass[] starTimes;
	public starClass stars;

	public Text timeText;
	public playerController playerCon;
	public pathFinderCube pathFinder;
	public Button peekButton;
	public Text peekText;

	public Text coinEarned;
	public Sprite emptyStar;
	public Sprite normalStar;

	public levelGraphics graphs;
	mazeCoding mazeCoder;

	string starDataStr;
	int playedLevelInt;

	void Start () {
		playedLevelInt = PlayerPrefs.GetInt ("playedLevel");
		if (!tutorialLevel)
			spawnGraphics ();
		else
			openPopup ();
		mazeCoder = GetComponent<mazeCoding> ();
        if (tutorialLevel)
            playedLevelInt = 60;
        if (!tutorialLevel)
        {
			if (playedLevelInt == 0) {
				peekTutorial.SetActive (true);
				Time.timeScale = 0;
			}
            peekCount = peekCounts[mazeCoder.levelNum / 10];
            canPeek = true;
            peekText.text = String.Format("Peek ({0})", peekCount);
            plusTime = starTimes[playedLevelInt / 10].star3time;
            time = plusTime;
            timeText.text = String.Format("{0}:{1:00}", ((int)time / 60), (int)(time % 60));
        }
        collected = 0;
		win = false;

   		if (turnNo > 0) {
			started = true;
			Time.timeScale = 1f;
		}
		stars.star1.SetActive(false);
		stars.star2.SetActive(false);
		stars.star3.SetActive(false);
	}
		
	void Update () {
		if (Input.GetKeyDown (KeyCode.G) && Input.GetKey (KeyCode.H))
			resetProggress ();
		if (canStart && fader.fin) {
			SceneManager.LoadScene(0);
			canStart = false;
		}
		if (canRestart && fader.fin) {
			canRestart = false;
			SceneManager.LoadScene(1);
		}
		else  {
			if(!win && !gameLost) // lose da ekle
				time = plusTime - Time.timeSinceLevelLoad;
			if (time < 0 && tutorialLevel == false) {
				time = 0;
				loseGame ();
			}
            if (tutorialLevel == false && time > starTimes[playedLevelInt / 10].star2time && tutorialLevel == false)
            {
				stars.starText.text = String.Format ("{0}:{1:00}", ((int)starTimes[playedLevelInt/10].star2time / 60), (int)(starTimes[playedLevelInt/10].star2time % 60));
				stars.starTimestar1.sprite = normalStar;
				stars.starTimestar2.sprite = normalStar;
				stars.starTimestar3.sprite = normalStar;
            }
            else if (tutorialLevel == false && time > starTimes[playedLevelInt / 10].star1time)
            {
				stars.starTimestar3.sprite = normalStar;
				stars.starTimestar2.sprite = normalStar;
				stars.starTimestar1.sprite = emptyStar;
				stars.starText.text = String.Format ("{0}:{1:00}", ((int)starTimes[playedLevelInt/10].star1time / 60), (int)(starTimes[playedLevelInt/10].star1time % 60));
            }
            else if (tutorialLevel == false && time != 0)
            {
				stars.starTimestar3.sprite = normalStar;
				stars.starTimestar2.sprite = emptyStar;
				stars.starTimestar1.sprite = emptyStar;
				stars.starText.text = "0:00";
			} 
			else if(tutorialLevel == false)
				stars.starTimestar3.sprite = emptyStar;

			timeDeb = time;

			if (Input.GetKeyDown (KeyCode.Q))
				Time.timeScale = 30f;
			if(Input.GetKeyUp (KeyCode.Q))
				Time.timeScale = 1f;
			
			timeText.text = String.Format ("{0}:{1:00}", ((int)time / 60), (int)(time % 60));

			if (win)
				wining ();
		}
	}

	public void spawnGraphics()
	{
		if (PlayerPrefs.GetInt ("tutorial") == 0) {
			GameObject obj;
			if (playedLevelInt < 11) {
				obj = Instantiate(graphs.s1Objects ,new Vector3(36.6f,0f,38.5f),Quaternion.Euler(0,0,0)) as GameObject;
				trailRenderer.GetComponent<TrailRenderer> ().material = graphs.s1trailMaterial;
			} else if (playedLevelInt < 20) {
				obj = Instantiate(graphs.s2Objects ,new Vector3(38.1f,-49.4f,67.2f),Quaternion.Euler(90,0,0)) as GameObject;
				trailRenderer.GetComponent<TrailRenderer> ().material = graphs.s1trailMaterial;
			} else if (playedLevelInt < 30) {
				obj = Instantiate(graphs.s3Objects ,new Vector3(36.6f,0f,38.5f),Quaternion.Euler(0,0,0)) as GameObject;
				trailRenderer.GetComponent<TrailRenderer> ().material = graphs.s3trailMaterial;
			} else if (playedLevelInt < 40) {
				obj = Instantiate(graphs.s4Objects ,new Vector3(36.6f,0f,38.5f),Quaternion.Euler(0,0,0)) as GameObject;
				trailRenderer.GetComponent<TrailRenderer> ().material = graphs.s3trailMaterial;
			} else if (playedLevelInt < 50) {
				obj = Instantiate(graphs.s5Objects ,new Vector3(36.6f,0f,38.5f),Quaternion.Euler(0,0,0)) as GameObject;
				trailRenderer.GetComponent<TrailRenderer> ().material = graphs.s5trailMaterial;
			} else if (playedLevelInt < 60) {
				obj = Instantiate(graphs.s6Objects ,new Vector3(36.6f,0f,38.5f),Quaternion.Euler(0,0,0)) as GameObject;
				trailRenderer.GetComponent<TrailRenderer> ().material = graphs.s5trailMaterial;
			}
		}
	}

	public void wining()
	{
			youWinScreen.SetActive(true);
		if (playedLevelInt == 19 || playedLevelInt == 39 || playedLevelInt == 59)
			nextButton.SetActive (false);

			if (starIncrease)
			{
				starDataStr = PlayerPrefs.GetString("starData");
				//3 yıldız -------------------
				if (tutorialLevel || time >= starTimes[playedLevelInt / 10].star2time)
				{
					stars.star1.SetActive(true);
					stars.star2.SetActive(true);
					stars.star3.SetActive(true);

					PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") + 30 + collected * 2);
					coinEarned.text = "Earend Coin: " + (30 + collected * 2).ToString();
					if (!tutorialLevel)
					{
						if (playedLevelInt == starDataStr.Length)
						{ //yeni bölüm
							PlayerPrefs.SetString("starData", starDataStr + "3");
						}
						else if (playedLevelInt == starDataStr.Length - 1)
						{ //son bölüm tekrar
							PlayerPrefs.SetString("starData", (starDataStr.Substring(0, playedLevelInt) + "3"));
						}
						else
						{ //eski bölümler
							PlayerPrefs.SetString("starData", (starDataStr.Substring(0, playedLevelInt) + "3" + starDataStr.Substring(playedLevelInt + 1, starDataStr.Length - (playedLevelInt + 1))));
						}
						if (int.Parse(PlayerPrefs.GetString("starData").Substring(playedLevelInt, 1)) < 3)
							PlayerPrefs.SetFloat("stars", PlayerPrefs.GetFloat("stars") + (3 - int.Parse(PlayerPrefs.GetString("starData").Substring(playedLevelInt, 1))));

						playedLevelInt = PlayerPrefs.GetInt("playedLevel");
					}
					//2 yıldız -------------------
				}
				else if (time >= starTimes[playedLevelInt / 10].star1time)
				{
					stars.star1.SetActive(true);
					stars.star2.SetActive(true);

					PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") + 20 + collected * 2);
					coinEarned.text = "Earend Coin: " + (20 + collected * 2).ToString();

					if (playedLevelInt == starDataStr.Length)
					{ //yeni bölüm
						PlayerPrefs.SetString("starData", starDataStr + "2");
					}
					else if (playedLevelInt == starDataStr.Length - 1 && int.Parse(starDataStr.Substring(playedLevelInt, 1)) < 2)
					{ //son bölüm tekrar
						PlayerPrefs.SetString("starData", (starDataStr.Substring(0, playedLevelInt) + "2"));
					}
					else if (int.Parse(starDataStr.Substring(playedLevelInt, 1)) < 2)
					{ //eski bölümler
						PlayerPrefs.SetString("starData", (starDataStr.Substring(0, playedLevelInt) + "2" + starDataStr.Substring(playedLevelInt + 1, starDataStr.Length - (playedLevelInt + 1))));
					}

					if (int.Parse(PlayerPrefs.GetString("starData").Substring(playedLevelInt, 1)) < 2)
						PlayerPrefs.SetFloat("stars", PlayerPrefs.GetFloat("stars") + (2 - int.Parse(PlayerPrefs.GetString("starData").Substring(playedLevelInt, 1))));
					playedLevelInt = PlayerPrefs.GetInt("playedLevel");

					//1 yıldız -------------------
				}
				else if (time > 0)
				{
					stars.star1.SetActive(true);

					PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") + 10 + collected * 2);
					coinEarned.text = "Earend Coin: " + (10 + collected * 2).ToString();

					if (playedLevelInt == starDataStr.Length)
					{ //son bölüm
						PlayerPrefs.SetString("starData", starDataStr + "1");
					}
					else if (playedLevelInt == starDataStr.Length - 1 && int.Parse(starDataStr.Substring(playedLevelInt, 1)) < 1)
					{ //restart
						PlayerPrefs.SetString("starData", (starDataStr.Substring(0, playedLevelInt) + "1"));
					}
					else if (int.Parse(starDataStr.Substring(playedLevelInt, 1)) < 1)
					{ //eski bölümler
						PlayerPrefs.SetString("starData", (starDataStr.Substring(0, playedLevelInt) + "1" + starDataStr.Substring(playedLevelInt + 1, starDataStr.Length - (playedLevelInt + 1))));
					}

					playedLevelInt = PlayerPrefs.GetInt("playedLevel");

					if (int.Parse(PlayerPrefs.GetString("starData").Substring(playedLevelInt, 1)) < 1)
						PlayerPrefs.SetFloat("stars", PlayerPrefs.GetFloat("stars") + (1 - int.Parse(PlayerPrefs.GetString("starData").Substring(playedLevelInt, 1))));

					//0 yıldız -------------------
				}
				else
				{
					PlayerPrefs.SetInt("coins", collected * 2);
					coinEarned.text = "Earend Coin: " + (collected * 2).ToString();

					if (playedLevelInt == starDataStr.Length)
					{ //yeni bölüm
						PlayerPrefs.SetString("starData", starDataStr + "0");
					}
					else if (playedLevelInt == starDataStr.Length - 1 && int.Parse(starDataStr.Substring(playedLevelInt, 1)) == 0)
					{ //son bölüm tekrar
						PlayerPrefs.SetString("starData", (starDataStr.Substring(0, playedLevelInt) + "0"));
					}
					else if (int.Parse(starDataStr.Substring(playedLevelInt, 1)) == 0)
					{ //eski bölümler
						PlayerPrefs.SetString("starData", (starDataStr.Substring(0, playedLevelInt) + "0" + starDataStr.Substring(playedLevelInt + 0, starDataStr.Length - (playedLevelInt + 0))));
					}
				}
				starIncrease = false;
			}
	}
		
	public void coinSoundPlay()
	{
		coinSound.Play ();
	}

	public void medalSoundPlay()
	{
		medalSound.Play ();
	}

	public void restartGame(){
		canRestart = true;
		fader.blackToWhite = false;
		fader.gameObject.GetComponent<Image> ().enabled = true;
		fader.opened = true;
		fader.fin = false;
	}

	public void start(){
		started = true;
		Time.timeScale = 1f;
	}
	public void pause(){
        if (!win)
        {
            pathFinder.waitForPlayersMove = true;
            Time.timeScale = 0f;
            pauseScreen.SetActive(true);
        }
	}
	public void resume(){
		Time.timeScale = 1f;
        pathFinder.waitForPlayersMove = false;
	}

    public void returnMenu()
    {
		canStart = true;
		fader.blackToWhite = false;
		fader.opened = false;
		fader.fin = false;
    }

	public void peek(){
        pathFinder.trailRenderer.Clear();
		if (peekCount > 0) {
			peekCount--;
			peekText.text = String.Format ("Peek ({0})", peekCount);
		}
		if (peekCount == 0)
			canPeek = false;
	}
	public void nextLevel(){
		
		canRestart = true;

		PlayerPrefs.SetInt ("playedLevel", PlayerPrefs.GetInt("playedLevel") + 1);
		fader.blackToWhite = false;
		fader.gameObject.GetComponent<Image> ().enabled = true;
		fader.opened = true;
		fader.fin = false;
	}

	public void loseGame(){
		if (playerCon) {
			gameLost = true;
			GetComponent<AudioSource> ().volume = 0;
			slimeDieSound.Play ();
			playerCon.die ();
			playerCon.moveStart = false;
			pathFinder.pathBlock = true;
			Destroy (playerCon);

			Invoke ("openLoseScreen", 2f);
		}
	}

	public void openLoseScreen(){
		loseSound.Play ();
		loseScreen.SetActive (true);
	}

	public void resetProggress()
	{
		PlayerPrefs.SetInt("tutorial", 1);
		PlayerPrefs.SetString ("starData", "");
		PlayerPrefs.SetInt("bought0", 1);
		PlayerPrefs.SetInt("bought1", 0);
		PlayerPrefs.SetInt("bought2", 0);
		PlayerPrefs.SetInt("bought3", 0);
		PlayerPrefs.SetInt("coins", 0);
		PlayerPrefs.SetFloat("stars", 0);
		PlayerPrefs.SetInt("coins", 0);
	}

	public void openPopup(){
		popUpOn = true;
	}

	public void closePopup(){
		popUpOn = false;
	}
}
