﻿using UnityEngine;
using System.Collections;

public class coin : MonoBehaviour {

    private GameManager gamemanager;
	public bool taken = false;
	Transform point;
	public bool medal = false;
	camera cam;
	public GameObject textEffect;
	TextMesh textEff;
	GameObject textEffObj;
	public Color TextColor;
		
	void OnTriggerEnter (Collider col) {
		if (col.gameObject.name == "player" && !taken) {
			
			if (medal) { //medal
				coinMove sc = gameObject.AddComponent<coinMove>( ) as coinMove;
				sc.isMedal = true;
				GameManager.collected++;
				GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameManager> ().plusTime += 3f;
				taken = true;
			} 
			else {  // coin
				coinMove sc = gameObject.AddComponent<coinMove>( ) as coinMove;
				sc.isMedal = false;
				sc.textEffect = textEffect ;
				sc.TextColor = TextColor;

				GameManager.collected++;
				GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameManager> ().plusTime += 3f;
				taken = true;
			}
		}	
	}
}
