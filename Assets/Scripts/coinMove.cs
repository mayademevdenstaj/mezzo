﻿using UnityEngine;
using System.Collections;

public class coinMove : MonoBehaviour {

	Transform point;
	camera cam;
	public GameObject textEffect;
	public TextMesh textEff;
	public GameObject textEffObj;
	public Color TextColor; // 255,255,255,255 may not be variable
	float memCol = 1f;
	SpriteRenderer sprt;
	public bool isMedal;

	void Start()
	{
		
			sprt = GetComponent<SpriteRenderer> ();
			cam = Camera.main.gameObject.GetComponent<camera> ();
			point = GameObject.FindGameObjectWithTag ("coinpoint").transform;
		if (!isMedal) {
			GameObject txtEff = Instantiate (textEffect, new Vector3 (transform.position.x - 2f, 1f, transform.position.z - 0.5f), new Quaternion (0, 0, 0, 1f)) as GameObject;
			textEff = txtEff.GetComponent<TextMesh> ();
			textEffObj = txtEff;
			textEff.transform.localEulerAngles = new Vector3 (90, 0, 0);
		}
	}


	void Update () {
		if (textEff && !isMedal) {
			textEff.transform.Translate (0, 0.05f, 0);
			memCol = Mathf.Lerp (memCol, 0f, 0.03f);
			TextColor = new Color (TextColor.r, TextColor.g, TextColor.b, memCol);
			textEff.color = TextColor;
			if (memCol < 0.1) {
				GameObject.Destroy (textEffObj.gameObject);
				GameObject.Destroy (this.gameObject);
			}
		}
		if (cam.viev)
			sprt.enabled = false;
		else
			sprt.enabled = true;
		transform.position = Vector3.MoveTowards(transform.position, point.transform.position, 1f);
	}
}
