﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class menu : MonoBehaviour {

    int charNo = 4;
    int currentChar;
    public int[] prices;
    public Text boughtText;
    public GameObject locks;
    public Text coinCount;
    public GameObject image;
    public Sprite image1;
    public Sprite image2;
    public Sprite image3;
    public Sprite image4;
    public Sprite[] images;
    public GameObject warning;
	bool canStart = false;
	public sceneTrans screenFader;
	public ScrollRect myScrollRect;
	public static float scrollData = 1;
    public Button okbutton;
	public Text buttonText;

    void Start()
    {
        if (PlayerPrefs.GetInt("currentChar") == 0)
            PlayerPrefs.SetInt("currentChar", 1);
        currentChar = PlayerPrefs.GetInt("currentChar");
	    images = new Sprite[charNo];
	    images[0] = image1;
	    images[1] = image2;
	    images[2] = image3;
	    images[3] = image4;
	    prices = new int[charNo];
		prices [0] = 20;
		prices [1] = 150;
		prices [2] = 500;
		prices [3] = 1500;
	    PlayerPrefs.SetInt("bought0", 1);
	    screenUpdate();
		if (levelSelect.levelSelectOpen == true) {
		    myScrollRect.verticalNormalizedPosition = scrollData;
		}
    }
		
	void Update(){
		if (canStart && screenFader.fin) {
			
			canStart = false;
			SceneManager.LoadScene (1);
		}
	}
		
    public void right()
    {
        if (currentChar < charNo)
        {
            currentChar++;
            warning.SetActive(false);
            screenUpdate();
        }
    }

    public void left()
    {
        if (currentChar > 1)
        {
            currentChar--;
            warning.SetActive(false);
            screenUpdate();
        }
    }

    public void screenUpdate()
    {
		coinCount.text = PlayerPrefs.GetInt("coins").ToString();
		if (scrollData != 0.0f)
			myScrollRect.verticalNormalizedPosition = scrollData;
        string name = string.Format("bought{0}", currentChar - 1);
        image.GetComponent<Image>().sprite = images[currentChar - 1];
        if (PlayerPrefs.GetInt(name) == 0) // not bought
        {
            boughtText.text = prices[currentChar - 1].ToString();
            locks.SetActive(true);
            okbutton.interactable = false;
			buttonText.text = "Buy";
        }
        else  // bought
        {
			if (PlayerPrefs.GetInt ("currentChar") != currentChar)
				buttonText.text = "Select";
			else
				buttonText.text = "Selected";
			
            boughtText.text = "Bought";
            locks.SetActive(false);
            okbutton.interactable = true;

        }
    }

    public void startButton()
    {
		
		canStart = true;
		screenFader.blackToWhite = false;
		screenFader.opened = false;
		screenFader.fin = false;
    }

    public void buyButton()
	{
		if (buttonText.text == "Buy") {
			if (PlayerPrefs.GetInt (string.Format ("bought{0}", currentChar - 1)) == 0) {
				if (prices [currentChar - 1] <= PlayerPrefs.GetInt ("coins")) {
					PlayerPrefs.SetInt ("coins", PlayerPrefs.GetInt ("coins") - prices [currentChar - 1]);
            
					string name = string.Format ("bought{0}", currentChar - 1);
					PlayerPrefs.SetInt (name, 1);
					screenUpdate ();
					buttonText.text = "Selected";
					PlayerPrefs.SetInt("currentChar", currentChar);
				} else
					warning.SetActive (true);
			}
		} 
		else if (buttonText.text == "Select")
		{
			buttonText.text = "Selected";
			PlayerPrefs.SetInt("currentChar", currentChar);
		
		}
    }

	public void unlockLevels()
	{
        PlayerPrefs.SetString("starData", "323232123332132132122332212332123332123321233321233213333213");
		PlayerPrefs.SetInt ("coins", 1000);
		PlayerPrefs.SetInt("tutorial", 0);
		SceneManager.LoadScene (0);
	}

    public void reset()
    {
        PlayerPrefs.SetInt("tutorial", 1);
		PlayerPrefs.SetString ("starData", "");
        PlayerPrefs.SetInt("bought0", 1);
        PlayerPrefs.SetInt("bought1", 0);
        PlayerPrefs.SetInt("bought2", 0);
        PlayerPrefs.SetInt("bought3", 0);
		PlayerPrefs.SetInt("coins", 0);
        PlayerPrefs.SetFloat("stars", 0);
        PlayerPrefs.SetInt("coins", 0);
        PlayerPrefs.SetInt("currentChar", 1);
    }
    
	public void scrolling()
	{
		scrollData = myScrollRect.verticalNormalizedPosition;
	}
     
}
