﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class sceneTrans : MonoBehaviour {

	public bool blackToWhite;
	Image img;
	public bool fin = false;
	public bool opened;
	public bool inAndOut;

	void Start () {
		img = GetComponent <Image> ();
	}

	void Update () {
		if (inAndOut && blackToWhite) {
			blackToWhite = false;
			opened = true;
			fin = false;
		}
		if (inAndOut && !blackToWhite && !opened) {
			blackToWhite = true;
			inAndOut = false;
			opened = true;
			fin = false;
		}

		if (opened) {
			if (blackToWhite)
				img.color = new Color (0, 0, 0, Mathf.Lerp (img.color.a, 0f, 0.1f));
			else
				img.color = new Color (0, 0, 0, Mathf.Lerp (img.color.a, 1f, 0.1f));
			if (blackToWhite && img.color.a < 0.1f)
				fin = true;
			else if (!blackToWhite && img.color.a > 0.99f)
				fin = true;
		}
		if (fin && blackToWhite && !inAndOut)
			img.enabled = false;
		if (fin)
			opened = false;
	}
	public void startFade(){
		opened = true;
		img.enabled = true;
	}
}
