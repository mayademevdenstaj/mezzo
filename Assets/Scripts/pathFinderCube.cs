﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class pathFinderCube : MonoBehaviour {

	public int tut;

	public playerController player;
	public TrailRenderer trailRenderer;
	public float lineSpeed = 100f;
	bool pressDown= false;
	bool pressUp= false;
	public camera cam;
	public Vector3 mous;
	public bool pathBlock = false;



	Vector2 firstPressPos;
	Vector2 secondPressPos;
	Vector2 currentSwipe;

	int once = 0;

	Ray upRay;
	Ray downRay;
	Ray rightRay;
	Ray leftRay;

	RaycastHit hit;

	public Vector2 finalSwipe;
	public float minDistance;

	bool canGo = false;

	public bool f;
    public bool b;
    public bool r;
    public bool l;

	public bool waitForPlayersMove = false;
	public bool firstTut;

	public void setProb(){
		f = Physics.Raycast (upRay, out hit, minDistance);
		b = Physics.Raycast (downRay, out hit, minDistance);
		r = Physics.Raycast (rightRay, out hit, minDistance);
		l = Physics.Raycast (leftRay, out hit, minDistance);
	}

	void Start(){
		
	}

    void Update()
    {
		if (GameManager.popUpOn)
			pathBlock = true;

		tut = PlayerPrefs.GetInt ("tutorial");

        if (PlayerPrefs.GetInt("controls") == 1) //Swipe Control
        {
            player.moveStart = true;
            trailRenderer.enabled = false;
            if(canGo==false)
                Swipe();

			Debug.DrawRay(transform.position, Vector3.forward * minDistance, Color.red);
			Debug.DrawRay(transform.position, Vector3.right * minDistance, Color.red);
			Debug.DrawRay(transform.position, Vector3.left * minDistance, Color.red);
			Debug.DrawRay(transform.position, Vector3.back * minDistance, Color.red);
			upRay = new Ray(transform.position, Vector3.forward * minDistance);
			downRay = new Ray(transform.position, Vector3.back * minDistance);
			rightRay = new Ray(transform.position, Vector3.right * minDistance);
			leftRay = new Ray(transform.position, Vector3.left * minDistance);


            if (once == 0)
            {
                //yukarı
                if (finalSwipe == new Vector2(0, 1))
                {
                    if (Physics.Raycast(upRay, out hit, minDistance) || (r == true && Physics.Raycast(rightRay, out hit, minDistance) == false) || (l == true && Physics.Raycast(leftRay, out hit, minDistance) == false))
                    {
						//Debug.Log("yukarı" + " kapalı");
                        canGo = false;
                        finalSwipe = new Vector2(0, 0);
                    }
                    else
                        canGo = true;
                }
                //asagi
                if (finalSwipe == new Vector2(0, -1))
                {
                    if (Physics.Raycast(downRay, out hit, minDistance) || (r == true && Physics.Raycast(rightRay, out hit, minDistance) == false) || (l == true && Physics.Raycast(leftRay, out hit, minDistance) == false))
                    {
						//Debug.Log("asagi" + " kapalı");
                        canGo = false;
                        finalSwipe = new Vector2(0, 0);
                    }
                    else
                        canGo = true;
                }
                //saga
                if (finalSwipe == new Vector2(1, 0))
                {
                    if (Physics.Raycast(rightRay, out hit, minDistance) || (f == true && Physics.Raycast(upRay, out hit, minDistance) == false) || (b == true && Physics.Raycast(downRay, out hit, minDistance) == false))
                    {
						//Debug.Log("sag" + " kapalı");
                        canGo = false;
                        finalSwipe = new Vector2(0, 0);
                    }
                    else
                        canGo = true;
                }
                //sola
                if (finalSwipe == new Vector2(-1, 0))
                {
                    if (Physics.Raycast(leftRay, out hit, minDistance) || (f == true && Physics.Raycast(upRay, out hit, minDistance) == false) || (b == true && Physics.Raycast(downRay, out hit, minDistance) == false))
                    {
						//Debug.Log("sol" + " kapalı");
                        canGo = false;
                        finalSwipe = new Vector2(0, 0);
                    }
                    else
                        canGo = true;
                }
            }

            setProb();
            transform.Translate(new Vector3(finalSwipe.x * 2f, 0, finalSwipe.y * 2f));
        }
		else //Draw Control
        {
            trailRenderer.enabled = true;

            if (Input.GetKeyDown(KeyCode.Mouse0) && !player.levelEnd && !cam.viev && !pathBlock)
            { 
                trailRenderer.Clear();
                lineSpeed = 1000f;
                player.moveStart = false;
                player.gameOver = false;
                player.crashed = false;
                pressUp = false;
                pressDown = true;
                trailRenderer.time = lineSpeed;
				GameObject temp =  playerController.path [playerController.currentPath];
				playerController.path = playerController.path.GetRange (0, playerController.currentPath);
				playerController.path.Add (temp);

            }
            if (Input.GetKey(KeyCode.Mouse0) && player.moveStart == false && Time.timeSinceLevelLoad > 0.1f && !pathBlock)
            {
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				mousePos = new Vector3(mousePos.x, transform.position.y, mousePos.z);
                transform.position = mousePos;

            }
            if (Input.GetKeyUp(KeyCode.Mouse0) && !pathBlock)
            {
                lineSpeed = playerController.path.Count * 0.4f;
                trailRenderer.time = lineSpeed;
                player.moveStart = true;
                pressUp = true;
                pressDown = false;
            }
            if (pressUp && lineSpeed > 0.8)
            {
                if (lineSpeed > 10)
                    lineSpeed = lineSpeed * 0.8f;
                else
                    lineSpeed = lineSpeed * 0.99f;
                trailRenderer.time = lineSpeed;
            }
            if (pressDown && lineSpeed < 30)
            {
                if (lineSpeed < 5)
                    lineSpeed = lineSpeed * 1.01f;
                else
                    lineSpeed = lineSpeed * 1.001f;
                trailRenderer.time = lineSpeed;
            }
        }

		pathBlock = false;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Trigger(Clone)" && (Time.timeSinceLevelLoad > 0.1f || (col.gameObject.transform.position.x == 0f && col.gameObject.transform.position.z == 0f)) && !pathBlock && !waitForPlayersMove)
        {
			playerController.path.Add (col.gameObject);
		} 
    }
	public void dontAddPath(){
		pathBlock = true;
	}
	public void addPath(){
		pathBlock = false;
	}
	public void brokeBlock(){
		StartCoroutine (waitSome());
	}
	IEnumerator waitSome(){
		yield return new WaitForSeconds (0.2f);
		addPath ();
	}

	public void firstTutFix()
	{
		firstTut = true;
		Invoke ("firstTutFix2", 0.3f);
	}
	public void firstTutFix2()
	{
		firstTut = false;
	}

	public void Swipe()
	{
		if (Time.timeScale != 0 && !firstTut) {

			player.crashed = false;
			if (Input.GetMouseButtonDown (0)) {
				//save began touch 2d point
				firstPressPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
			}
			if (Input.GetMouseButtonUp (0)) {
				//save ended touch 2d point
				secondPressPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);

				//create vector from the two points
				currentSwipe = new Vector2 (secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

				//up
				if (currentSwipe.y >= currentSwipe.x && currentSwipe.y >= 0 && currentSwipe.y + currentSwipe.x > 0 && !pathBlock)
					finalSwipe = new Vector2 (0, 1);
				
				//down
				if (currentSwipe.y <= currentSwipe.x && currentSwipe.y <= 0 && currentSwipe.y + currentSwipe.x < 0 && !pathBlock)
					finalSwipe = new Vector2 (0, -1);
				
				//left
				if (currentSwipe.y >= currentSwipe.x && currentSwipe.x <= 0 && currentSwipe.y + currentSwipe.x < 0 && !pathBlock)
					finalSwipe = new Vector2 (-1, 0);
				
				//right
				if (currentSwipe.y <= currentSwipe.x && currentSwipe.x >= 0 && currentSwipe.y + currentSwipe.x > 0 && !pathBlock)
					finalSwipe = new Vector2 (1, 0);
			}
		}
		if(Input.GetKeyDown(KeyCode.UpArrow))
			finalSwipe = new Vector2 (0, 1);
		if(Input.GetKeyDown(KeyCode.DownArrow))
			finalSwipe = new Vector2 (0, -1);
		if(Input.GetKeyDown(KeyCode.RightArrow))
			finalSwipe = new Vector2 (1, 0);
		if(Input.GetKeyDown(KeyCode.LeftArrow))
			finalSwipe = new Vector2 (-1, 0);
	}
    public void updateControls(){
		waitForPlayersMove = true;
	}
	public void centerPose(){
		transform.position = new Vector3 (Mathf.Round (player.transform.position.x),transform.position.y,Mathf.Round (player.transform.position.z));
	}
}
