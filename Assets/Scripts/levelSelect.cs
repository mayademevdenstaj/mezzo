﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class levelSelect : MonoBehaviour {

	public levelSelectData[] levels;
	int currentLevel = 0;
	public menu menuObject;
	public sceneTrans sceneFader;
	public string startdat;
	public GameObject season2Unlocked;
	public GameObject season3Unlocked;
	public Text starCount;
	public static bool levelSelectOpen;

	public Sprite emptyStar;
	public Sprite lockedLevel;

    void Start()
    {
        updateStars();
    }



	public void startGame (GameObject pressed) {
		if (pressed.GetComponent<levelSelectData> ().locked == false) {
			levelSelectOpen = true;
			PlayerPrefs.SetInt ("playedLevel", currentLevel + int.Parse (pressed.transform.GetChild(0).GetComponent<Text>().text) - 1);	
			menuObject.startButton ();
			sceneFader.startFade ();
		}
	}

	int calculateStars(string data)
	{
		int total = 0;
		for (int i = 0; i < data.Length; i++) {
			total+= int.Parse(data.Substring(i,1));
		}
		return total;
	}

	public void levelBack()
	{
		levelSelectOpen = false;
	}

	public void updateStars(){
		levelSelectOpen = true;
		startdat = PlayerPrefs.GetString("starData");
		int totalStars = calculateStars (startdat);
		starCount.text = totalStars.ToString();

		for (int i = 0; i < 60;i++) {
			if (i < startdat.Length) {
				if (startdat [i] == '1') {
					levels [i].star2.sprite = emptyStar;
					levels [i].star3.sprite = emptyStar;
				}
				if (startdat [i] == '2') {
					levels [i].star3.sprite = emptyStar;
				}
			}
			if (i == startdat.Length) {
				if (i == 20 && totalStars < 45 || i == 40 && totalStars < 90) {
					if (i == 20 && totalStars < 45)
						season2Unlocked.GetComponent<Image> ().sprite = lockedLevel;
					else if (i == 40 && totalStars < 90)
						season3Unlocked.GetComponent<Image> ().sprite = lockedLevel;

					levels [i].GetComponent<Image> ().sprite = lockedLevel;
					levels [i].locked = true;
					levels [i].star1.sprite = emptyStar;
					levels [i].star2.sprite = emptyStar;
					levels [i].star3.sprite = emptyStar;
				}
				else {
					levels [i].star1.sprite = emptyStar;
					levels [i].star2.sprite = emptyStar;
					levels [i].star3.sprite = emptyStar;
				}
			}

			if (i > startdat.Length) {
				if(i<=20)
					season2Unlocked.GetComponent<Image> ().sprite = lockedLevel;
				else if(i<=40)
					season3Unlocked.GetComponent<Image> ().sprite = lockedLevel;
				levels [i].GetComponent<Image> ().sprite = lockedLevel;
				levels [i].locked = true;
				levels [i].star1.sprite = emptyStar;
				levels [i].star2.sprite = emptyStar;
				levels [i].star3.sprite = emptyStar;
			}
		}
	}
}
