﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class camera : MonoBehaviour {

	bool tutLevel = false;
	public float smooth;
	public Transform focusPos;
	public Transform player;
	Camera cam;
	pathFinderCube pth;
	int levNum = 0;
	public float camSize;
	public mazeCoding gameMng;

	[System.Serializable]
	public class camOptions
	{
		public float maxY;
		public float maxX;
		public Vector2 vievPos;
		public float peekSize;
	}
	public camOptions[] cameraOptions;

	public float minX;
	public float minY;

	public bool viev;
	public mazeCoding mazeGenerater;

	void Start(){
		if (PlayerPrefs.GetInt ("tutorial") == 0)
			tutLevel = true;

		if (tutLevel) {
			minX = -1000;
			minY = -1000;
		}

		levNum = PlayerPrefs.GetInt("playedLevel");
		/*if (gameMng && gameMng.debugMode)
			levNum = gameMng.levelNum;*/
		pth = focusPos.GetComponent<pathFinderCube> ();
		cam = GetComponent<Camera> ();
		if (mazeGenerater && mazeGenerater.size == 6) {
			cameraOptions[levNum / 10].maxX = 12f;
			cameraOptions[levNum / 10].maxY = 12f;
		}else if(mazeGenerater && mazeGenerater.size == 10){
			cameraOptions[levNum / 10].maxX = 30f;
			cameraOptions[levNum / 10].maxY = 25.7f;
        }
        else if (!mazeGenerater) {
            levNum = 60;
        }
	}

	void Update () {
		if(!viev)																																							
        	transform.position = new Vector3(Mathf.Lerp(transform.position.x, player.position.x, smooth), 10f, Mathf.Lerp(transform.position.z, player.position.z, smooth));
	}
	void LateUpdate(){
		if (viev) {

			int playedLevelInt = PlayerPrefs.GetInt ("playedLevel"); 
			if (playedLevelInt < 11) {
				cam.orthographicSize = Mathf.Lerp(cam.orthographicSize,cameraOptions[0].peekSize,0.1f);
				transform.position = new Vector3(Mathf.Lerp(transform.position.x, cameraOptions[0].vievPos.x, smooth), 10f, Mathf.Lerp(transform.position.z, cameraOptions[0].vievPos.y, smooth));
			}
			else if (playedLevelInt < 21) {
				cam.orthographicSize = Mathf.Lerp(cam.orthographicSize,cameraOptions[1].peekSize,0.1f);
				transform.position = new Vector3(Mathf.Lerp(transform.position.x, cameraOptions[1].vievPos.x, smooth), 10f, Mathf.Lerp(transform.position.z, cameraOptions[1].vievPos.y, smooth));
			}
			else if (playedLevelInt < 31) {
				cam.orthographicSize = Mathf.Lerp(cam.orthographicSize,cameraOptions[2].peekSize,0.1f);
				transform.position = new Vector3(Mathf.Lerp(transform.position.x, cameraOptions[2].vievPos.x, smooth), 10f, Mathf.Lerp(transform.position.z, cameraOptions[2].vievPos.y, smooth));
			}
			else if (playedLevelInt < 41) {
				cam.orthographicSize = Mathf.Lerp(cam.orthographicSize,cameraOptions[3].peekSize,0.1f);
				transform.position = new Vector3(Mathf.Lerp(transform.position.x, cameraOptions[3].vievPos.x, smooth), 10f, Mathf.Lerp(transform.position.z, cameraOptions[3].vievPos.y, smooth));
			}
			else if (playedLevelInt < 51) {
				cam.orthographicSize = Mathf.Lerp(cam.orthographicSize,cameraOptions[4].peekSize,0.1f);
				transform.position = new Vector3(Mathf.Lerp(transform.position.x, cameraOptions[4].vievPos.x, smooth), 10f, Mathf.Lerp(transform.position.z, cameraOptions[4].vievPos.y, smooth));
			}
			else if (playedLevelInt < 60) {
				cam.orthographicSize = Mathf.Lerp(cam.orthographicSize,cameraOptions[5].peekSize,0.1f);
				transform.position = new Vector3(Mathf.Lerp(transform.position.x, cameraOptions[5].vievPos.x, smooth), 10f, Mathf.Lerp(transform.position.z, cameraOptions[5].vievPos.y, smooth));
			}
	
		} 
		else {
			cam.orthographicSize = Mathf.Lerp(cam.orthographicSize,camSize,0.2f);
			if (transform.position.x < minX)
				transform.position = new Vector3 (minX, 50f, transform.position.z);
			if (transform.position.x > cameraOptions[levNum / 10].maxX)
				transform.position = new Vector3 (cameraOptions[levNum / 10].maxX, 50f, transform.position.z);

			if (transform.position.z > cameraOptions[levNum / 10].maxY)
				transform.position = new Vector3 (transform.position.x, 50f, cameraOptions[levNum / 10].maxY);
			if (transform.position.z < minY)
				transform.position = new Vector3 (transform.position.x, 50f, minY);
		}
	}
	public void vievOn(){
		if(GameManager.canPeek)
			viev = true;
	}

	public void vievOff(){
		viev = false;
	}
}
